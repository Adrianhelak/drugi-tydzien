public class AccountTester {

	public static void main(String[] args) {
		Account account = new Account(1234, 900);
		System.out.println(account.getBalance());
		account.deposit(500);
		System.out.println(account.getBalance());
		try {
			account.withdraw(3000);
		} catch (InsufficientFundsException e) {
			System.out.println(e.getMessage());
		}
		System.out.println(account.getBalance());
		System.out.println(account);
	}
}